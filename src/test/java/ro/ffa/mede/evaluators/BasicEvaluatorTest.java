package ro.ffa.mede.evaluators;

import org.junit.Assert;
import org.junit.Test;

public class BasicEvaluatorTest {

    private static final int[] trueLabels =     {0, 1, 1, 0};
    private static final int[] guessedLabels =  {0, 1, 1, 1};

    @Test
    public void testSimpleConfusionMatrix() {
        final int[][] confusionMatrix = BasicEvaluator.getConfusionMatrix(trueLabels, guessedLabels);
        final int[][] expectedMatrix = {
                {1, 0},
                {1, 2}
        };

        Assert.assertArrayEquals(expectedMatrix[0], confusionMatrix[0]);
        Assert.assertArrayEquals(expectedMatrix[1], confusionMatrix[1]);
    }

    @Test
    public void testStatistics() {
        final int[][] confusionMatrix = {
                {5, 2, 0},
                {3, 3, 2},
                {0, 1, 11}
        };

        final Statistics stats = BasicEvaluator.stats(confusionMatrix);
        Assert.assertEquals(0.7037, stats.getAccuracy(), 0.00009);
        Assert.assertEquals(0.6687, stats.getPrecision(), 0.00009);
        Assert.assertEquals(0.6571, stats.getRecall(), 0.00009);
        Assert.assertEquals(0.6584, stats.getF1Score(), 0.00009);
    }


}
