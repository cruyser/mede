package ro.ffa.mede.data;

import org.junit.Test;
import ro.ffa.mede.extractors.utils.ExtractorType;
import ro.ffa.mede.extractors.FeatureExtractor;

import java.nio.file.Paths;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DataAggregatorTest {

    @Test
    public void simpleAggregatorTest() throws Exception{
        final DataAggregator aggregator = new DataAggregator.Builder()
                .withRootDataFolderPath(String.valueOf(Paths.get(this.getClass().getResource("/images").toURI())))
                .withRandom(new Random(12))
                .withTrainPercentage(0.5)
                .withNormalized(false)
                .withFeatureExtractor(FeatureExtractor.of(ExtractorType.RGB_CONCATENATED, 2, 2, 2))
                .build();

        final AggregatedData aggregatedData = aggregator.aggregateData(false);

        assertTrue(aggregatedData.getLabelsMap().containsKey("agricultural"));
        assertTrue(aggregatedData.getLabelsMap().containsKey("airplane"));
        assertTrue(aggregatedData.getLabelsMap().containsKey("beach"));

        assertEquals(3, aggregatedData.getTrainData().size());
        assertEquals(3, aggregatedData.getTestData().size());
    }

}
