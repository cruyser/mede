package ro.ffa.mede.data.helper;

import org.junit.Assert;
import org.junit.Test;
import ro.ffa.mede.data.DataSet;
import ro.ffa.mede.extractors.utils.ExtractorType;
import ro.ffa.mede.extractors.FeatureExtractor;

import java.nio.file.Paths;

public class ClassFolderReaderTest {

    @Test
    public void testSimpleAgricultural() throws Exception {
        final var agriculturalPath = Paths.get(this.getClass().getResource("/images/agricultural").toURI());

        final var reader = new ClassFolderReader(1, agriculturalPath);
        final DataSet dataSet = reader.extractDataSet(FeatureExtractor.of(ExtractorType.RGB_CONCATENATED, 1, 1, 1), false);

        Assert.assertEquals(2, dataSet.getFeatures().length);
        Assert.assertEquals(3, dataSet.getFeatures()[0].length);
        Assert.assertEquals(3, dataSet.getFeatures()[1].length);
        Assert.assertArrayEquals(new int[]{1, 1}, dataSet.getLabels());
    }

}
