package ro.ffa.mede.knn;

import org.junit.Assert;
import org.junit.Test;

public class KnnTest {

    private static final double[][] trainFeatures = {
            { 1,  2},
            {-1, -2}
    };
    private static final int[] trainLabels = {1, 2};

    @Test
    public void testSimpleClassification() {
        final var label = Knn.of(trainFeatures, trainLabels, 1).classify(new double[] {2, 4});
        Assert.assertEquals(1, label);
    }

}
