package ro.ffa.mede.knn;

import java.util.stream.IntStream;

public class Neighbour implements Comparable<Neighbour>{
    private final double distance;
    private final int label;

    public Neighbour(double distance, int label) {
        this.distance = distance;
        this.label = label;
    }

    @Override
    public int compareTo(Neighbour that) {
        return (int)Math.signum(this.distance - that.distance);
    }

    public static Neighbour[] createGroupFrom(double[] distances, int[] labels) {
        return IntStream
                .range(0, distances.length)
                .mapToObj(index -> new Neighbour(distances[index], labels[index]))
                .toArray(Neighbour[]::new);
    }

    public double getDistance() {
        return distance;
    }

    public int getLabel() {
        return label;
    }
}
