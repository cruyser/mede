package ro.ffa.mede.extractors;

import ro.ffa.mede.extractors.utils.ExtractorType;
import ro.ffa.mede.utils.Copyable;

import java.awt.image.BufferedImage;

public interface FeatureExtractor extends Copyable<FeatureExtractor> {
    static FeatureExtractor of(ExtractorType extractorType, int... bucketSizes) {
        switch (extractorType) {
            case RGB_CONCATENATED:  return new RgbFeatureExtractor(bucketSizes[0], bucketSizes[1], bucketSizes[2]);
            case HSV_CONCATENATED:  return new HsvFeatureExtractor(bucketSizes[0], bucketSizes[1], bucketSizes[2]);
            case GRAY_FROM_RGB:     return new RgbToGrayFeatureExtractor(bucketSizes[0]);
            case HUE_ONLY:          return new HueOnlyExtractor(bucketSizes[0]);
            default: throw new IllegalArgumentException("Unsupported extractor type");
        }
    }

    int[] extractCount(BufferedImage image);

    double[] extractNormalized(BufferedImage image);

}
