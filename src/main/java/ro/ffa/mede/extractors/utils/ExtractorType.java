package ro.ffa.mede.extractors.utils;

public enum ExtractorType {
    RGB_CONCATENATED, HSV_CONCATENATED, GRAY_FROM_RGB, HUE_ONLY
}
