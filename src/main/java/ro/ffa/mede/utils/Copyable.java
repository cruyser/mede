package ro.ffa.mede.utils;

public interface Copyable<T> {
    T copy();
}
