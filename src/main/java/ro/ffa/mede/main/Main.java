package ro.ffa.mede.main;

import ro.ffa.mede.data.AggregatedData;
import ro.ffa.mede.data.DataAggregator;
import ro.ffa.mede.data.DataSet;
import ro.ffa.mede.evaluators.BasicEvaluator;
import ro.ffa.mede.evaluators.Statistics;
import ro.ffa.mede.extractors.FeatureExtractor;
import ro.ffa.mede.extractors.utils.ExtractorType;
import ro.ffa.mede.knn.Knn;

import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws IOException {

        final DataAggregator aggregator = new DataAggregator.Builder()
                .withRandom(new Random(12))
                .withRootDataFolderPath("X:\\Downloads\\UCMerced_LandUse\\images")
                .withTrainPercentage(0.7)
                .withNormalized(true)
                .withFeatureExtractor(FeatureExtractor.of(ExtractorType.HSV_CONCATENATED, 180))
                .build();

        LOGGER.info("Start collecting data...");
        long ref = System.currentTimeMillis();
        final AggregatedData aggregatedData = aggregator.aggregateData(true);
        LOGGER.log(Level.INFO, "Finished reading data and extracting features: {0} seconds", (System.currentTimeMillis() - ref) / 1000);

        final DataSet trainSet = aggregatedData.getTrainData();
        final DataSet testSet = aggregatedData.getTestData();

        final Knn knn = Knn.of(trainSet.getFeatures(), trainSet.getLabels(), 7);
        LOGGER.info("Start classification...");
        ref = System.currentTimeMillis();
        final int[] guessedLabels = knn.classify(testSet.getFeatures());
        LOGGER.log(Level.INFO, "Finished classifying data: {0} seconds", (System.currentTimeMillis() - ref) / 1000);

        final Statistics stats =
                BasicEvaluator.stats(BasicEvaluator.getConfusionMatrix(testSet.getLabels(), guessedLabels));

        System.out.println(stats);
        System.out.println(aggregatedData.getLabelsMap());
    }

}
