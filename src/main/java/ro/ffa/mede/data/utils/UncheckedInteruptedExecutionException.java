package ro.ffa.mede.data.utils;

public class UncheckedInteruptedExecutionException extends RuntimeException{
    public UncheckedInteruptedExecutionException(Throwable cause) {
        super(cause);
    }
}
